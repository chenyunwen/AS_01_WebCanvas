# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Choose color or fill color                       | 1~5%      | Y         |
| Fill or not when texting or drawing shape        | 1~5%      | Y         |


---

### How to use 

    1. 選擇顏色
        (1) 可以選擇兩個部份的顏色，一個是畫筆、圖形外框、文字外框的顏色(主色Color)，另一個是圖形填滿、文字填滿的顏色。
        在選擇顏色時，透過按下正方形來決定要選的是Color還是Fill的顏色，選擇中的那個選項上方會打勾，下方色條以及箭頭會改變，方塊中的顏色為預覽現在選中的顏色。
        (2) 下方三個色條分別選擇色相、彩度、明度(h,s,l)，在選擇hsl時，除了h色條不會改變，s和l都會根據選到的顏色做出改變。(下方的brush size也會變色，和主色相同，因此選擇Fill時它不會跟著變)
        另外，h不能滑動，但s和l色條皆可由滑動選擇，不過游標不可超出色條範圍。
    2. 筆刷寬度
        (1) 左方會顯示現在的筆刷寬度，控制圖形、字體外框線條寬度和筆刷大小。
        (2)可由滑動或點選做修改，滑動時游標不可超出色條範圍。
    3. 文字大小
        (1) 左方會顯示現在的文字大小。
        (2) 可由拖動或點選做修改。
        p.s.在印文字時筆刷寬度建議調很小，不然印出的字會變一坨QQ
    4. 輸入文字
        (1) 在輸入框打字，右方可選擇字形。
        (2) 輸入完必須點擊右方"T"按鈕，之後在畫布上點選想要印出文字的地方。
    5. 下方按鈕依序分別為:
        畫筆、橡皮擦、redo、undo
        畫圓 畫長方形 畫三角形 控制實心與空心
        清空畫布 下載畫布圖片
    6. 實心與空心
        按下實心與空心按鈕後，有三種模式輪流變化:
        (1) 空心，只有外框，顏色為主色(icon為黑色外框中心白色)
        (2) 實心，顏色為Fill(icon為黑色塗滿)
        (3) 外框+實心塗滿，外框為主色，中間色為Fill(icon為灰色外框中間黑色)
        這個按鈕控制畫圖形時的模式，以及印出文字時的模式。(有無外框、填滿)


### Function description

    init():設定初始值
    mouse_down():在畫布按下滑鼠時的動作(裡面會判斷現在選中的功能)(mouse_up()、mouse_out()同理)
    mouse_move():判斷是否有按下滑鼠，並依據現在選中的功能實做不同行為。
    (其中有判斷fill為何種模式，因此變更畫出圖形的模式，空心或實心)
    cq_choice(c): 根據選擇Color或Fill的顏色做出反應(改變打勾位置以及色條顏色)
    s_md()~l_mo():根據在色條上的行為做出反應，以便滑動選色。
    change_color_hsl(new_color)~change_color_l(event):選擇hsl並改變其他物件的顯示。
    w_md()~w_mo():根據在物件上的行為做出反應，以便滑動選則筆刷寬度。
    change_lineWidth(event):改變筆刷寬度。
    hange_text_size():改變字體大小。
    changiing_function(value):按按鈕改變已選中的功能。
    change_fill_or_not():改變圖形是否要填滿。
    doing_texting(event):在畫布上印出字。
    clean_canvas():清除畫布。
    action_push():每做一個動作會呼叫他一次，儲存當下畫布上的圖片並放進一個array。
    action_undo()、action_redo():根據array中存放的資料實作undo和redo的功能。
    download_img():下載現在畫布上的圖片。


    
### Gitlab page link

    https://chenyunwen.gitlab.io/AS_01_WebCanvas/

### Others (Optional)
    
    因為我的username不是學號，所以無法將page link設定成指定的形式，因此附上學號:107060003
    辛苦助教們了!

