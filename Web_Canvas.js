var board, contx;
var draw = false;
var option_now = "d";   //d=draw e=eraser t=text c=circle r=rectangle tr=triangle
var start_x, start_y;   //for drawing circle and rectangle
var fill = 1;           //1 = border  2 = fill 3 = border+fill

function init(){
    board = document.getElementById("myCanvas");
    contx = board.getContext("2d");
    contx.lineWidth = "1";
    contx.strokeStyle = "hsl(0, 100%, 50%)";
    contx.fillStyle = "hsl(0, 100%, 50%)";
    action_push();
    console.log("start");
}

function mouse_down(){
    
    if(option_now == "d"){
        contx.moveTo(event.offsetX, event.offsetY);
        draw = true;
        // contx.globalCompositeOperation = "source-over";
        contx.beginPath();
    } else if (option_now == "e"){
        draw = true;
        contx.globalCompositeOperation = "destination-out";
        contx.beginPath();
    } 
    // else if (option_now == "t"){
    //     doing_texting(event);
    // } 
    else if (option_now == "r" || option_now == "c" || option_now == "tr"){
        event.preventDefault();
        event.stopPropagation();
        start_x = parseInt(event.offsetX);
        start_y = parseInt(event.offsetY);
        draw = true;
    }
    
}

function mouse_move(){
    if(draw){
        if(option_now == "d"){
            contx.lineTo(event.offsetX, event.offsetY);
            contx.stroke();
        } else if (option_now == "e"){
            //contx.clearRect(event.offsetX, event.offsetY, contx.lineWidth, contx.lineWidth);
            contx.lineTo(event.offsetX, event.offsetY);
            contx.stroke();
        } else if (option_now == "r"){
            event.preventDefault();
            event.stopPropagation();
            var ori_img = new Image();
            ori_img.src = action_array[act_step];
            contx.clearRect( 0, 0, document.getElementById("myCanvas").width, document.getElementById("myCanvas").height );
            contx.drawImage(ori_img, 0, 0);
            var r_width = event.offsetX - start_x;
            var r_height = event.offsetY - start_y;
            if(fill == 1){
                contx.strokeRect(start_x,start_y,r_width,r_height);
            } else if (fill == 2){
                contx.fillRect(start_x,start_y,r_width,r_height);
            } else if (fill == 3){
                contx.fillRect(start_x,start_y,r_width,r_height);
                contx.strokeRect(start_x,start_y,r_width,r_height);
            }
            
        } else if (option_now == "c"){
            event.preventDefault();
            event.stopPropagation();
            var ori_img = new Image();
            ori_img.src = action_array[act_step];
            contx.clearRect( 0, 0, document.getElementById("myCanvas").width, document.getElementById("myCanvas").height );
            contx.drawImage(ori_img, 0, 0);
            var center_x = (event.offsetX + start_x)/2;
            var center_y = (event.offsetY + start_y)/2;
            var radius = Math.sqrt((event.offsetX - center_x)*(event.offsetX - center_x) + (event.offsetY - center_y)*(event.offsetY - center_y));
            contx.beginPath();
            contx.arc(center_x,center_y,radius,0*Math.PI,2*Math.PI);
            if(fill == 1){
                contx.stroke();
            } else if (fill == 2){
                contx.fill();
            } else if (fill == 3) {
                contx.fill();
                contx.stroke();
            }
            contx.closePath();
        } else if (option_now == "tr"){
            // event.preventDefault();
            // event.stopPropagation();
            var ori_img = new Image();
            ori_img.src = action_array[act_step];
            contx.clearRect( 0, 0, document.getElementById("myCanvas").width, document.getElementById("myCanvas").height );
            contx.drawImage(ori_img, 0, 0);
            var thire_point_x = start_x + start_x - event.offsetX;
            contx.beginPath();
            contx.moveTo(start_x, start_y);
            contx.lineTo(thire_point_x, event.offsetY);
            contx.lineTo(event.offsetX, event.offsetY);
            contx.lineTo(start_x, start_y);
            if(fill == 1){
                contx.stroke();
            } else if (fill == 2){
                contx.fill();
            }
            else if (fill == 3){
                contx.fill();
                contx.stroke();
            }
            contx.closePath();
        }
        
    }
}

function mouse_up(){
    
    if(option_now == "d" && draw){
        contx.closePath();
        draw = false;
        action_push();
    } else if (option_now == "e" && draw){
        contx.closePath();
        draw = false;
        contx.globalCompositeOperation = "source-over";
        action_push();
    } else if ( (option_now == "c" || option_now == "r" || option_now == "tr") && draw){
        event.preventDefault();
        event.stopPropagation();
        draw = false;
        action_push();
    }
    
}

function mouse_out(){
    if(draw == true) {
        draw = false;
        if(option_now == "d" || option_now == "e"){
            contx.closePath();
            contx.globalCompositeOperation = "source-over";
            action_push();
        }
        else if (option_now == "c" || option_now == "r" || option_now == "tr"){
            event.preventDefault();
            event.stopPropagation();
            action_push();
        }
    }
    
    
}

function mouse_click(event){
    if (option_now == "t"){
            doing_texting(event);
    }
}

//fill color or border color

var b_f_color = "b";

function cq_choice(c){
    if(c==1) {
        b_f_color = "b";
        var pos_1 = document.getElementById("huepointer");
        var num_1 = document.getElementById("hue_"+color_h).offsetLeft;
        pos_1.style.left = num_1+"px";

        var pos_2 = document.getElementById("huepointer_s");
        var num_2 = color_s*360/100;
        pos_2.style.left = num_2+"px";

        var pos_3 = document.getElementById("huepointer_l");
        var num_3 = color_l*360/100;
        pos_3.style.left = num_3+"px";

        document.getElementById("hue_s").style.background = "linear-gradient(to right,hsl("+color_h+", 0%, "+ color_l +"%) , hsl("+color_h+", 100%, "+ color_l +"%))";
        document.getElementById("hue_l").style.background = "linear-gradient(to right,hsl("+color_h+", "+ color_s +"%, 0%) , hsl("+color_h+", "+ color_s +"%, 50%) ,hsl("+color_h+", "+ color_s +"%, 100%))";
        
        document.getElementById("c_t").innerHTML = "v";
        document.getElementById("c_t_f").innerHTML = "";

    } else if (c==2) {
        b_f_color = "f";

        var pos_1 = document.getElementById("huepointer");
        var num_1 = document.getElementById("hue_"+color_h_f).offsetLeft;
        pos_1.style.left = num_1+"px";

        var pos_2 = document.getElementById("huepointer_s");
        var num_2 = color_s_f*360/100;
        pos_2.style.left = num_2+"px";

        var pos_3 = document.getElementById("huepointer_l");
        var num_3 = color_l_f*360/100;
        pos_3.style.left = num_3+"px";

        document.getElementById("hue_s").style.background = "linear-gradient(to right,hsl("+color_h_f+", 0%, "+ color_l_f +"%) , hsl("+color_h_f+", 100%, "+ color_l_f +"%))";
        document.getElementById("hue_l").style.background = "linear-gradient(to right,hsl("+color_h_f+", "+ color_s_f +"%, 0%) , hsl("+color_h_f+", "+ color_s_f +"%, 50%) ,hsl("+color_h_f+", "+ color_s_f +"%, 100%))";

        document.getElementById("c_t").innerHTML = "";
        document.getElementById("c_t_f").innerHTML = "v";
    }
    
}

//choose color

var color_h = 0;
var color_s = 100;
var color_l = 50;

var color_h_f = 0;
var color_s_f = 100;
var color_l_f = 50;

var ch_c = false;

function s_md(){
    ch_c = true;
}
function s_msm(event){
    if(ch_c){
        change_color_s(event);
    }
}
function s_mu(){
    ch_c = false;
}
function s_mo(){
    ch_c = false;
}

function l_md(){
    ch_c = true;
}
function l_msm(event){
    if(ch_c){
        change_color_l(event);
    }
}
function l_mu(){
    ch_c = false;
}
function l_mo(){
    ch_c = false;
}

function change_color_hsl(new_color){
    if(b_f_color == "b"){

        color_h = new_color;
        var pos = document.getElementById("huepointer");
        var num = document.getElementById("hue_"+color_h).offsetLeft;
        pos.style.left = num+"px";
        contx.strokeStyle = `hsl(${color_h}, ${color_s}%, ${color_l}%)`; 
        document.getElementById("hue_s").style.background = "linear-gradient(to right,hsl("+color_h+", 0%, "+ color_l +"%) , hsl("+color_h+", 100%, "+ color_l +"%))";
        document.getElementById("hue_l").style.background = "linear-gradient(to right,hsl("+color_h+", "+ color_s +"%, 0%) , hsl("+color_h+", "+ color_s +"%, 50%) ,hsl("+color_h+", "+ color_s +"%, 100%))";
        document.getElementById("color_square_1").style.background = "hsl("+color_h+", "+color_s+"%, "+color_l+"%)";
        document.getElementById("brush_size").style.borderColor = "transparent hsl("+color_h+", "+color_s+"%, "+color_l+"%)";
    
    } else if (b_f_color == "f"){

        color_h_f = new_color;
        var pos = document.getElementById("huepointer");
        var num = document.getElementById("hue_"+color_h_f).offsetLeft;
        pos.style.left = num+"px";
        contx.fillStyle = `hsl(${color_h_f}, ${color_s_f}%, ${color_l_f}%)`; 
        document.getElementById("hue_s").style.background = "linear-gradient(to right,hsl("+color_h_f+", 0%, "+ color_l_f +"%) , hsl("+color_h_f+", 100%, "+ color_l_f +"%))";
        document.getElementById("hue_l").style.background = "linear-gradient(to right,hsl("+color_h_f+", "+ color_s_f +"%, 0%) , hsl("+color_h_f+", "+ color_s_f +"%, 50%) ,hsl("+color_h_f+", "+ color_s_f +"%, 100%))";
        document.getElementById("color_square_2").style.background = "hsl("+color_h_f+", "+color_s_f+"%, "+color_l_f+"%)";
    
    }
    
}

function change_color_s(event){
    if(b_f_color == "b"){

        var pos = document.getElementById("huepointer_s");
        var num = event.offsetX;
        pos.style.left = num+"px";
        color_s = num*100/360;
        contx.strokeStyle = `hsl(${color_h}, ${color_s}%, ${color_l}%)`;
        document.getElementById("hue_l").style.background = "linear-gradient(to right,hsl("+color_h+", "+ color_s +"%, 0%) , hsl("+color_h+", "+ color_s +"%, 50%) ,hsl("+color_h+", "+ color_s +"%, 100%))";
        document.getElementById("color_square_1").style.background = "hsl("+color_h+", "+color_s+"%, "+color_l+"%)";
        document.getElementById("brush_size").style.borderColor = "transparent hsl("+color_h+", "+color_s+"%, "+color_l+"%)";

    } else if (b_f_color == "f"){

        var pos = document.getElementById("huepointer_s");
        var num = event.offsetX;
        pos.style.left = num+"px";
        color_s_f = num*100/360;
        contx.fillStyle = `hsl(${color_h_f}, ${color_s_f}%, ${color_l_f}%)`;
        document.getElementById("hue_l").style.background = "linear-gradient(to right,hsl("+color_h_f+", "+ color_s_f +"%, 0%) , hsl("+color_h_f+", "+ color_s_f +"%, 50%) ,hsl("+color_h_f+", "+ color_s_f +"%, 100%))";
        document.getElementById("color_square_2").style.background = "hsl("+color_h_f+", "+color_s_f+"%, "+color_l_f+"%)";

    }

}

function change_color_l(event){
    if(b_f_color == "b"){

        var pos = document.getElementById("huepointer_l");
        var num = event.offsetX;
        pos.style.left = num+"px";
        color_l = num*100/360;
        contx.strokeStyle = `hsl(${color_h}, ${color_s}%, ${color_l}%)`;
        document.getElementById("hue_s").style.background = "linear-gradient(to right,hsl("+color_h+", 0%, "+ color_l +"%) , hsl("+color_h+", 100%, "+ color_l +"%))";
        document.getElementById("color_square_1").style.background = "hsl("+color_h+", "+color_s+"%, "+color_l+"%)";
        document.getElementById("brush_size").style.borderColor = "transparent hsl("+color_h+", "+color_s+"%, "+color_l+"%)";
        
    } else if (b_f_color == "f"){

        var pos = document.getElementById("huepointer_l");
        var num = event.offsetX;
        pos.style.left = num+"px";
        color_l_f = num*100/360;
        contx.fillStyle = `hsl(${color_h_f}, ${color_s_f}%, ${color_l_f}%)`;
        document.getElementById("hue_s").style.background = "linear-gradient(to right,hsl("+color_h_f+", 0%, "+ color_l_f +"%) , hsl("+color_h_f+", 100%, "+ color_l_f +"%))";
        document.getElementById("color_square_2").style.background = "hsl("+color_h_f+", "+color_s_f+"%, "+color_l_f+"%)";

    }


}


//choose linewidth
var ch_w = false;
function w_md(){
    ch_w = true;
}
function w_msm(event){
    if(ch_w){
        change_lineWidth(event);
    }
}
function w_mu(){
    ch_w = false;
}
function w_mo(){
    ch_w = false;
}

function change_lineWidth(event){
    var pos = document.getElementById("pointer_bs");
    var num = event.offsetX;
    pos.style.left = num+"px";
    num/=4;
    num+=1;
    contx.lineWidth = ""+ num;
    num = Math.floor(num);
    document.getElementById("change_bsh").innerHTML = "Brush Size: "+num+"px";
}

//choose text size
var text_size = 50;
function change_text_size(){
    var ts = document.getElementById("text_size");
    text_size = ts.value;
    document.getElementById("t_label").innerHTML = "Text size: "+text_size+"px"
}

//buttons
function changiing_function(value){
    if(value == "pen") {
        option_now = "d";
        document.getElementById("myCanvas").style.cursor = "url(img/pen_cursor.png),auto";
    } else if (value == "eraser") {
        option_now = "e";
        document.getElementById("myCanvas").style.cursor = "url(img/eraser_cursor.png),auto";
    } else if (value == "text") {
        option_now = "t";
        document.getElementById("myCanvas").style.cursor = "text";
    } else if (value == "circle"){
        option_now = "c";
        document.getElementById("myCanvas").style.cursor = "url(img/circle_cursor.png),auto";
    } else if (value == "rectangle") {
        option_now = "r";
        document.getElementById("myCanvas").style.cursor = "url(img/rectangle_cursor.png),auto";
    } else if (value == "triangle") {
        option_now = "tr";
        document.getElementById("myCanvas").style.cursor = "url(img/triangle_cursor.png),auto";
    }
    console.log(option_now);
}

function change_fill_or_not(){
    var btn = document.getElementById("fillornot");
    if(fill == 1){
        btn.style.backgroundColor = "black";
        btn.style.border = "5px solid black";
        fill = 2;
    } else if (fill == 2){
        btn.style.backgroundColor = "black";
        btn.style.border = "5px solid gray";
        fill = 3;
    } else if (fill == 3 ){
        btn.style.backgroundColor = "white";
        btn.style.border = "5px solid black";
        fill = 1;
    }
}

function doing_texting(event){
    var input_text = document.getElementById("text_input");
    var font_face = document.getElementById("font_face");
    contx.font = text_size +"px " + font_face.value;
    if(fill == 1){
        contx.strokeText(input_text.value, event.offsetX, event.offsetY);
    } else if (fill == 2){
        contx.fillText(input_text.value, event.offsetX, event.offsetY);
    } else if (fill == 3){
        contx.fillText(input_text.value, event.offsetX, event.offsetY);
        contx.strokeText(input_text.value, event.offsetX, event.offsetY);
    }
    
    // contx.fillStyle
    // contx.fillText(input_text.value, event.offsetX, event.offsetY);
    action_push();
}

function clean_canvas(){
    contx.clearRect( 0, 0, document.getElementById("myCanvas").width, document.getElementById("myCanvas").height );
    //location.reload();
    action_push();
}

//undo and redo
var action_array = new Array();
var act_step = -1;
var max_pos = -1;

function action_push(){
    act_step++;
    max_pos++;
    if(act_step < max_pos){
        max_pos = act_step;
        action_array.length = max_pos;
    }
    action_array.push(document.getElementById("myCanvas").toDataURL("image/png"));
    // document.getElementById("demo").innerHTML = act_step;
    console.log(act_step + " acts  img:" + action_array.length);
}

function action_undo(){
    if(act_step > 0){
        // contx.globalCompositeOperation = "source-over";
        act_step--;
        var pre_img = new Image();
        pre_img.src = action_array[act_step];
        pre_img.onload = function () {
            // document.getElementById("demo").innerHTML = "ud2 "+act_step;
            contx.clearRect( 0, 0, document.getElementById("myCanvas").width, document.getElementById("myCanvas").height );
            contx.drawImage(pre_img, 0, 0);
        }
    }
}

function action_redo(){
    if(act_step < max_pos){
        // contx.globalCompositeOperation = "source-over";
        act_step++;
        var new_img = new Image();
        new_img.src = action_array[act_step];
        new_img.onload = function(){
            contx.clearRect( 0, 0, document.getElementById("myCanvas").width, document.getElementById("myCanvas").height );
            contx.drawImage(new_img, 0, 0);
        }
        // document.getElementById("demo").innerHTML = "rd "+act_step;
    }
}

// download image
function download_img(){
    console.log("download start");
    // event.herf = document.getElementById("myCanvas").toDataURL("image/png");
    // event.download = "your_drawing.png";
    var link = document.createElement('a');
    link.href = document.getElementById("myCanvas").toDataURL("image/png");
    link.download="my_drawing_OwO.png";
    link.click();
    console.log("download success");
}